
import './App.css';

function App() {
  return (
    <h1 className="text-3xl font-bold underline bg-red-300">
      Hello!
    </h1>
  );
}

export default App;
